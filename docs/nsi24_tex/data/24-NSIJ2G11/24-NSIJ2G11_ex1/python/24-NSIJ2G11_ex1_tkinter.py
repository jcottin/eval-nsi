class Chemin:
    def __init__(self, itineraire):
        self.itineraire = itineraire
        longueur, largeur = 0, 0
        for direction in self.itineraire:
            if direction == "D":
                longueur = longueur + 1
            if direction == "B":
                largeur = largeur +1
        self.longueur = longueur
        self.largeur = largeur
        self.grille = [['.' for i in range(longueur+1)] \
                       for j in range(largeur+1)]

    def remplir_grille(self):
        i, j = 0, 0 # Position initiale
        self.grille[0][0] = 'S' # Case de départ marquée d'un S
        for direction in self.itineraire:
            if direction == 'D':
                j += 1 # Déplacement vers la droite
            elif direction == 'B':
                i += 1 # Déplacement vers le bas
            self.grille[i][j] = '*' # Marquer le chemin avec '*'
        self.grille[self.largeur][self.longueur] = 'E' # Case d'arrivée marquée d'un E

    def tracer_chemin(self):
        for i in range(len(self.grille)):
            for j in range(len(self.grille[i])):
                if self.grille[i][j] != '.':
                    print(self.grille[i][j], end = ' ')
                else:
                    print(' ', end = ' ')
            print()
            
            
            
            
from random import choice

def itineraire_aleatoire(m, n):
    itineraire = ''
    i, j = 0, 0
    while i != m and j != n :
        deplacement = choice(['B', 'D'])
        itineraire += deplacement
        if deplacement == 'D':
            j += 1
        else:
            i+=1
    if i == m:
        itineraire = itineraire + 'D'*(n-j)
    if j == n:
        itineraire = itineraire + 'B'*(m-i)
    return itineraire               
            
def nombre_chemins(m, n):
    if m ==  1 or n == 1:
        return 1
    else:
        return nombre_chemins(m-1,n)+nombre_chemins(m, n-1)


   
chemin_1 = Chemin("DDBDBBDDDDB")
a = chemin_1.largeur
b = chemin_1.longueur

chemin_1.remplir_grille()    
chemin_1.tracer_chemin()   

chem = Chemin(itineraire_aleatoire(8,6))
chem.remplir_grille()
chem.tracer_chemin()

print( nombre_chemins(4, 2) )