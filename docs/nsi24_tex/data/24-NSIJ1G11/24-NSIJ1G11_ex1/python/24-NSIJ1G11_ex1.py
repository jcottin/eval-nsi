import random

# question 1 puis 3
voisins = [[1, 2, 3, 4, 5],
           [0, 2, 3],
           [0, 1,5],
           [0, 1],
           [0],
           [0, 2]]


def voisin_alea(voisins, s):
    return voisins[s][random.randrange(len(voisins[s]))]

print(voisin_alea(voisins, 0))

# question 4
def marche_alea(voisins, i, n):
    if n == 0:
        return i
    else:
        return marche_alea(voisins, voisin_alea(voisins, i), n-1)
    
def simule(voisins, i, n_test, n_pas):
    results = [0]*len(voisins)
    for k in range(n_test):
        s = marche_alea(voisins, i, n_pas)
        results[s] += 1
    for k in range(len(voisins)):
        results[k] = results[k]/n_test
    return results

print(simule(voisins, 0, 1000, 1000))
    