class Processus:
    """ Crée un processus de nom <nom> et de durée <duree> (exprimée en
    cycles d'ordonnancement) """    
    
    def __init__(self, nom:str, duree:int):
        self.nom = nom
        self.duree = duree
        
    def execute_un_cycle(self):
        """ Exécute le processus donné pendant un cycle."""
        self.duree -= 1
    
    def est_fini(self):
        """ Renvoie True si le processus est terminé, False sinon. """
        if self.duree == 0:
            return True
        return False
    
class File:
    def __init__(self):
        """ Crée une file vide """
        self.contenu = []

    def enfile(self, element):
        """ Enfile element dans la file """
        self.contenu.append(element)
        
    def defile(self):
        """ Renvoie le premier élément de la file et l'enlève de
        la file """
        if self.est_vide():
            return None
        return self.contenu.pop(0)

    def est_vide(self):    
        """ Renvoie True si la file est vide, False sinon """
        return self.contenu == []
    
class Ordonnanceur:
    def __init__(self):
        self.temps = 0
        self.file = File()

    def ajoute_nouveau_processus(self, proc):
        '''Ajoute un nouveau processus dans la file de
        l'ordonnanceur. '''
        self.file.enfile(proc)
        
    def tourniquet(self):
        '''Effectue une étape d'ordonnancement et renvoie le nom
        du processus élu.'''
        self.temps += 1
        if not self.file.est_vide():
            proc = self.file.defile()
            proc.execute_un_cycle()
            if not proc.est_fini():
                self.ajoute_nouveau_processus(proc)
            return proc.nom
        else:
            return None

if __name__ == "__main__":
    p1 = Processus("p1", 4)
    p2 = Processus("p2", 3)
    p3 = Processus("p3", 5)
    p4 = Processus("p4", 3)
    depart_proc = {0 : p1, 1 : p3, 2 : p2, 3 : p4}
    
    o = Ordonnanceur()
    cycle = 0
    if cycle in depart_proc:
        o.ajoute_nouveau_processus(depart_proc[cycle])
    processus = o.tourniquet()
    while processus != None:
        print(processus)
        cycle = cycle + 1
        if cycle in depart_proc:
            o.ajoute_nouveau_processus(depart_proc[cycle])
        processus = o.tourniquet()