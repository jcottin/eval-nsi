# Sujets de BAC 2024 en LaTeX

Un clic sur :  
- le nom du sujet ouvre le pdf  
- ![yes](./data/logo_yes.png) télécharge le zip contenant le tex et les dossiers nécessaires à la compilation de l'exercice  
- ![no](./data/logo_no.png) ne fait rien !

| 2024        | Exercice 1 | Exercice 2 | Exercice 3 |
|:-----------:|:----------:|:----------:|:----------:|
| [24-sujet0-a](./data/24-sujet0-a/24-sujet0-a.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/23-sujet0-a/23-sujet0-a_ex3.zip) |  
| [24-sujet0-b](./data/24-sujet0-b/24-sujet0-b.pdf) | [![yes](./data/logo_yes.png)](./data/24-sujet0-b/24-sujet0-b_ex1.zip) | [![yes](./data/logo_yes.png)](./data/24-sujet0-b/24-sujet0-b_ex2.zip) | [![yes](./data/logo_yes.png)](./data/24-sujet0-b/24-sujet0-b_ex3.zip) |  
| [24-NSIJ1G11](./data/24-NSIJ1G11/24-NSIJ1G11.pdf) | [![yes](./data/logo_yes.png)](./data/24-NSIJ1G11/24-NSIJ1G11_ex1.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ2G11](./data/24-NSIJ2G11/24-NSIJ2G11.pdf) | [![yes](./data/logo_yes.png)](./data/24-NSIJ2G11/24-NSIJ2G11_ex1.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ1JA1](./data/24-NSIJ1JA1/24-NSIJ1JA1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ2JA1](./data/24-NSIJ2JA1/24-NSIJ2JA1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ1AN1](./data/24-NSIJ1AN1/24-NSIJ1AN1.pdf) | [![yes](./data/logo_yes.png)](./data/24-NSIJ1AN1/24-NSIJ1AN1_ex1.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ2AN1](./data/24-NSIJ2AN1/24-NSIJ2AN1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ1ME1](./data/24-NSIJ1ME1/24-NSIJ1ME1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ2ME1](./data/24-NSIJ2ME1/24-NSIJ2ME1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ1ME3](./data/24-NSIJ1ME3/24-NSIJ1ME3.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ2ME3](./data/24-NSIJ2ME3/24-NSIJ2ME3.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ1PO1](./data/24-NSIJ1PO1/24-NSIJ1PO1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [24-NSIJ2PO1](./data/24-NSIJ2PO1/24-NSIJ2PO1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
