# Sujets de BAC 2022 en LaTeX

Un clic sur :  
- le nom du sujet ouvre le pdf  
- ![yes](./data/logo_yes.png) télécharge le zip contenant le tex et les dossiers nécessaires à la compilation de l'exercice  
- ![no](./data/logo_no.png) ne fait rien !

| 2022        | Exercice 1 | Exercice 2 | Exercice 3 | Exercice 4 | Exercice 5 |
|:-----------:|:----------:|:----------:|:----------:|:----------:|:----------:|
| [22-NSIJ1AS1](./data/22-NSIJ1AS1/22-NSIJ1AS1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/22-NSIJ1AS1/22-NSIJ1AS1_ex3.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ2AS1](./data/22-NSIJ2AS1/22-NSIJ2AS1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/22-NSIJ2AS1/22-NSIJ2AS1_ex4.zip) | ![no](./data/logo_no.png) |
| [22-NSIJ1AN1](./data/22-NSIJ1AN1/22-NSIJ1AN1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/22-NSIJ1NS1/22-NSIJ1AN1_ex5.zip)  |
| [22-NSIJ2AN1](./data/22-NSIJ2AN1/22-NSIJ2AN1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ1G11](./data/22-NSIJ1G11/22-NSIJ1G11.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ2G11](./data/22-NSIJ2G11/22-NSIJ2G11.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ1JA1](./data/22-NSIJ1JA1/22-NSIJ1JA1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ2JA1](./data/22-NSIJ2JA1/22-NSIJ2JA1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [22-NSIJ1LR1](./data/22-NSIJ1LR1/22-NSIJ1LR1.pdf) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/22-NSIJ1LR1/22-NSIJ1LR1_ex2.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [22-NSIJ2LR1](./data/22-NSIJ2LR1/22-NSIJ2LR1.pdf) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/22-NSIJ2LR1/22-NSIJ2LR1_ex2.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ1ME1](./data/22-NSIJ1ME1/22-NSIJ1ME1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/22-NSIJ1ME1/22-NSIJ1ME1_ex5.zip) |
| [22-NSIJ2ME1](./data/22-NSIJ2ME1/22-NSIJ2ME1.pdf) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/22-NSIJ2ME1/22-NSIJ2ME1_ex2.zip) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/22-NSIJ2ME1/22-NSIJ2ME1_ex4.zip) | ![no](./data/logo_no.png) |
| [22-NSIJ1ME3](./data/22-NSIJ1ME3/22-NSIJ1ME3.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ2ME3](./data/22-NSIJ2ME3/22-NSIJ2ME3.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ1NC1](./data/22-NSIJ1NC1/22-NSIJ1NC1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ2NC1](./data/22-NSIJ2NC1/22-NSIJ2NC1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [22-NSIJ1PO1](./data/22-NSIJ1PO1/22-NSIJ1PO1.pdf) | [![yes](./data/logo_yes.png)](./data/22-NSIJ1PO1/22-NSIJ1PO1_ex1.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
