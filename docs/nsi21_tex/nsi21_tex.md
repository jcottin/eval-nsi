# Sujets de BAC 2021 en LaTeX

Un clic sur :  
- le nom du sujet ouvre le pdf  
- ![yes](./data/logo_yes.png) télécharge le zip contenant le tex et les dossiers nécessaires à la compilation de l'exercice  
- ![no](./data/logo_no.png) ne fait rien !

| 2021        | Exercice 1 | Exercice 2 | Exercice 3 | Exercice 4 | Exercice 5 |
|:-----------:|:----------:|:----------:|:----------:|:----------:|:----------:|
| [21-sujet0](./data/21-sujet0/21-sujet0.pdf) | [![yes](./data/logo_yes.png)](./data/21-sujet0/21-sujet0_ex1.zip) | [![yes](./data/logo_yes.png)](./data/21-sujet0/21-sujet0_ex2.zip) | [![yes](./data/logo_yes.png)](./data/21-sujet0/21-sujet0_ex3.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |	
| [21-NSIJ1AN1](./data/21-NSIJ1AN1/21-NSIJ1AN1.pdf)  | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [21-NSIJ1G11](./data/21-NSIJ1G11/21-NSIJ1G11.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/21-NSIJ1G11/21-NSIJ1G11_ex4.zip) | ![no](./data/logo_no.png) |	
| [21-NSIJ2G11](./data/21-NSIJ2G11/21-NSIJ2G11.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [21-NSIJ1ME1](./data/21-NSIJ1ME1/21-NSIJ1ME1.pdf) | [![yes](./data/logo_yes.png)](./data/21-NSIJ1ME1/21-NSIJ1ME1_ex1.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/21-NSIJ1ME1/21-NSIJ1ME1_ex5.zip) |	
| [21-NSIJ2ME1](./data/21-NSIJ2ME1/21-NSIJ2ME1.pdf) | [![yes](./data/logo_yes.png)](./data/21-NSIJ2ME1/21-NSIJ2ME1_ex1.zip) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/21-NSIJ2ME1/21-NSIJ2ME1_ex5.zip) |
| [21-NSIJ1ME2](./data/21-NSIJ1ME2/21-NSIJ1ME2.pdf) | [![yes](./data/logo_yes.png)](./data/21-NSIJ1ME2/21-NSIJ1ME2_ex1.zip) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/21-NSIJ1ME2/21-NSIJ1ME2_ex3.zip) | [![yes](./data/logo_yes.png)](./data/21-NSIJ1ME2/21-NSIJ1ME2_ex4.zip) | ![no](./data/logo_no.png) |
| [21-NSIJ2ME2](./data/21-NSIJ2ME2/21-NSIJ2ME2.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [21-NSIJ1ME3](./data/21-NSIJ1ME3/21-NSIJ1ME3.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | [![yes](./data/logo_yes.png)](./data/21-NSIJ1ME3/21-NSIJ1ME3_ex4.zip) | ![no](./data/logo_no.png) |
| [21-NSIJ2ME3](./data/21-NSIJ2ME3/21-NSIJ2ME3.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |
| [21-NSIJ2PO1](./data/21-NSIJ2PO1/21-NSIJ2PO1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |

