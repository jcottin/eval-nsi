# Logiciel evalNSI - MàJ 07 oct. 24

**OS utilisé : Ubuntu**

[Lien de téléchargement](https://nuage02.apps.education.fr/index.php/s/diedFYZRHKr9jEH)

![L'interface](./data/demo.png)  

- nécessite une distribution LaTeX
- mes paquets sty sont das le dossier du même nom ; ils sont à placer dans vos dossiers personnels

## Comment ça marche ?

Dans le dossier récupérer, éxécuter la commande `python3 main.py`

### Panneau de sélection d'une année

- Choisir une année, les sujets existants apparaissent dans la liste à gauche : `Liste des sujets`
- Choisir un sujet :
* la version pdf du sujet s'affiche dans une nouvelle fenêtre à droite
* les thèmes des exercices s'affichent dans le panneau central

### Panneau de sélection d'un/des exercice(s)

- Cocher le/les exercice(s) du sujet à insérer puis cliquer sur `Valider la sélection`
- Recommencer, éventuellement avec un autre sujet

**Si un exercice existe en tex, celui ci est insérer sinon c'est le pdf issu du sujet qui l'est**

### Panneau de configuration et de compilation

- Choisir un type d'entête (DS, BAC blanc, ...)
- La date 
- Le numéro de l'évaluation
- La durée de l'évalutaion

- Cliquer sur `Créer TeX` : le fichier source ainsi que tous les fichiers sont créer dans le dossier `tmp_NSI` du dossier personnel
- Cliquer sur `Compiler` : éxecute la commande `pdflatex -synctex=1 -shell-escape -interaction=nonstopmode` surle fichier TeX créé
Lorsque la compilation est terminé, le bouton `Affiche PDF` permet d'afficher le résultat (si la compilation s'est bien passée)
- La bouton `Quitter` efface les fichiers inutiles du `tmp_NSI` en conservant le .tex, le .pdf et les fichiers 


## À venir dans les prochaines versions

- À terme, les sujets, exercices par exercices seront tous en tex
- La liste des sujets sélectionnés sera modifiable dans la liste : il sera possible de supprimé un exercice
- les corrections seront mises à jour
- Les fichiers .sty vont être simplifiés
- Les fichiers python seront commentés
- ... 

