# Sujets de BAC 2023 en LaTeX

Un clic sur :  
- le nom du sujet ouvre le pdf  
- ![yes](./data/logo_yes.png) télécharge le zip contenant le tex et les dossiers nécessaires à la compilation de l'exercice  
- ![no](./data/logo_no.png) ne fait rien !

| 2023        | Exercice 1 | Exercice 2 | Exercice 3 |
|:-----------:|:----------:|:----------:|:----------:|
| [23-sujet0-a](./data/23-sujet0-a/23-sujet0-a.pdf) | [![yes](./data/logo_yes.png)](./data/23-sujet0-a/23-sujet0-a_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-sujet0-a/23-sujet0-a_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-sujet0-a/23-sujet0-a_ex3.zip) |	
| [23-sujet0-b](./data/23-sujet0-b/23-sujet0-b.pdf) | [![yes](./data/logo_yes.png)](./data/23-sujet0-b/23-sujet0-b_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-sujet0-b/23-sujet0-b_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-sujet0-b/23-sujet0-b_ex3.zip) |
| [23-NSIJ1G11](./data/23-NSIJ1G11/23-NSIJ1G11.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1G11/23-NSIJ1G11_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1G11/23-NSIJ1G11_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1G11/23-NSIJ1G11_ex3.zip) |
| [23-NSIJ2G11](./data/23-NSIJ2G11/23-NSIJ2G11.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2G11/23-NSIJ2G11_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2G11/23-NSIJ2G11_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2G11/23-NSIJ2G11_ex3.zip) |
| [23-NSIJ1LI1](./data/23-NSIJ1LI1/23-NSIJ1LI1.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1LI1/23-NSIJ1LI1_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1LI1/23-NSIJ1LI1_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1LI1/23-NSIJ1LI1_ex3.zip) |
| [23-NSIJ2LI1](./data/23-NSIJ2LI1/23-NSIJ2LI1.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2LI1/23-NSIJ2LI1_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2LI1/23-NSIJ2LI1_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2LI1/23-NSIJ2LI1_ex3.zip) |
| [23-NSIJ1LR1](./data/23-NSIJ1LR1/23-NSIJ1LR1.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1LR1/23-NSIJ1LR1_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1LR1/23-NSIJ1LR1_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1LR1/23-NSIJ1LR1_ex3.zip) |
| [23-NSIJ2LR1](./data/23-NSIJ2LR1/23-NSIJ2LR1.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2LR1/23-NSIJ2LR1_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2LR1/23-NSIJ2LR1_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2LR1/23-NSIJ2LR1_ex3.zip) |
| [23-NSIJ1ME1](./data/23-NSIJ1ME1/23-NSIJ1ME1.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1ME1/23-NSIJ1ME1_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1ME1/23-NSIJ1ME1_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1ME1/23-NSIJ1ME1_ex3.zip) | 
| [23-NSIJ2ME1](./data/23-NSIJ2ME1/23-NSIJ2ME1.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2ME1/23-NSIJ2ME1_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2ME1/23-NSIJ2ME1_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2ME1/23-NSIJ2ME1_ex3.zip) |
| [23-NSIJ1PO1](./data/23-NSIJ1PO1/23-NSIJ1PO1.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1PO1/23-NSIJ1PO1_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1PO1/23-NSIJ1PO1_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ1PO1/23-NSIJ1PO1_ex3.zip) |
| [23-NSIJ2PO1](./data/23-NSIJ2PO1/23-NSIJ2PO1.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2PO1/23-NSIJ2PO1_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2PO1/23-NSIJ2PO1_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2PO1/23-NSIJ2PO1_ex3.zip) |
| [23-NSIJ2JA1](./data/23-NSIJ2JA1/23-NSIJ2JA1.pdf) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2JA1/23-NSIJ2JA1_ex1.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2JA1/23-NSIJ2JA1_ex2.zip) | [![yes](./data/logo_yes.png)](./data/23-NSIJ2JA1/23-NSIJ2JA1_ex3.zip) |  
| [23-NSIJ1AS1](./data/23-NSIJ1AS1/23-NSIJ1AS1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [23-NSIJ2AS1](./data/23-NSIJ2AS1/23-NSIJ2AS1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [23-NSIJ1AN1](./data/23-NSIJ1AN1/23-NSIJ1AN1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  
| [23-NSIJ2AN1](./data/23-NSIJ2AN1/23-NSIJ2AN1.pdf) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) | ![no](./data/logo_no.png) |  

